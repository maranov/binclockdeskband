﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BinClockDeskBand
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class BinClock : UserControl
    {
        public BinClock()
        {
            InitializeComponent();
            InitializeTimer();
            DataContext = DataContextInstance;
        }

        private ClockModel DataContextInstance = new ClockModel();

        private void InitializeTimer()
        {
            UpdateTime();

            DispatcherTimer clockTimer = new System.Windows.Threading.DispatcherTimer();
            clockTimer.Tick += ClockTimer_Tick;
            clockTimer.Interval = new TimeSpan(0, 0, 1);
            clockTimer.Start();
        }

        private void ClockTimer_Tick(object sender, EventArgs e)
        {
            UpdateTime();
        }

        private void UpdateTime()
        {
            var now = DateTime.Now;
            DataContextInstance.HourHigh = (uint)now.Hour / 10;
            DataContextInstance.HourLow = (uint)now.Hour % 10;
            DataContextInstance.MinuteHigh = (uint)now.Minute / 10;
            DataContextInstance.MinuteLow = (uint)now.Minute % 10;
            DataContextInstance.DisplayedDate = now.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
        }
    }

    public class ClockModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private uint hourHigh;
        private uint hourLow;
        private uint minuteHigh;
        private uint minuteLow;
        private string displayedDate;

        public uint HourHigh
        {
            get => hourHigh;
            set
            {
                hourHigh = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HourHigh)));
            }
        }

        public uint HourLow
        {
            get => hourLow;
            set
            {
                hourLow = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HourLow)));
            }
        }

        public uint MinuteHigh
        {
            get => minuteHigh;
            set
            {
                minuteHigh = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MinuteHigh)));
            }
        }

        public uint MinuteLow
        {
            get => minuteLow;
            set
            {
                minuteLow = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MinuteLow)));
            }
        }

        public string DisplayedDate
        {
            get => displayedDate;
            set
            {
                displayedDate = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DisplayedDate)));
            }
        }
    }
}
