# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

## [0.2.0] -- 2019-10-17

### Added
* Added a proper readme with installation instructions
* Added a toolbar with date

### Changed
* Set a minimum size
* Limited minutes to 3 bits (removed unused bit)

### Removed

## [0.1.0] -- 2019-10-28
A working concept
