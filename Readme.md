# Binary clock Windows DeskBand
A [Binary clock](https://en.wikipedia.org/wiki/Binary_clock) for your Windows Taskbar. For practicing your binary or just for some nerd cred. 😎

![Screenshot](./Sceenshot.png)

## Installation
Find the latest [Release](https://gitlab.com/maranov/binclockdeskband/-/releases) and download the archive.

![](./Install1.png)

Extract the archive somewhere and run the `Install.bat` as Administrator.

![](./Install2.png)

Right click on the Taskbar and enable the Binary clock.

![](./Install3.png)

Right click on the Taskbar again if for some reason the Binary clock doesn't appear in the menu immediately.

You can unlock the taskbar in the same menu to be able to move the clock around, then lock it again.

## Build
Everything is contained in the `BinClockDeskBand` solution. The `BinClockWindow` can be used for debugging.

Make sure to checkout the dsafa's `CSDeskband`, provided as a Git submodule in the root directory.

On release update the changelog, assembly info version, create a Git tag and attach a zip with the library and un/install batches to the tag in GitLab.

## Credits
Thanks to *dsafa* and *Johny* for providing me with code and help.
