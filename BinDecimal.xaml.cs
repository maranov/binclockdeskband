﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BinClockDeskBand
{
    /// <summary>
    /// Interaction logic for BinDecimal.xaml
    /// </summary>
    public partial class BinDecimal : UserControl
    {
        public BinDecimal()
        {
            InitializeComponent();
        }

        public static Brush DisabledGray = new SolidColorBrush(Color.FromRgb(0x2C, 0x2C, 0x2C));

        private static DependencyProperty ValueProperty = DependencyProperty.Register(
            nameof(Value),
            typeof(uint),
            typeof(BinDecimal),
            new FrameworkPropertyMetadata(DisplayValue));

        private static DependencyProperty MaxProperty = DependencyProperty.Register(
            nameof(Max),
            typeof(uint),
            typeof(BinDecimal),
            new FrameworkPropertyMetadata(LimitBits));

        private static void DisplayValue(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (BinDecimal)d;
            var value = (uint)e.NewValue;
            control.BitOne.Fill = (value % 2 == 1) ? Brushes.White : DisabledGray;
            control.BitTwo.Fill = ((value >> 1) % 2 == 1) ? Brushes.White : DisabledGray;
            control.BitThree.Fill = ((value >> 2) % 2 == 1) ? Brushes.White : DisabledGray;
            control.BitFour.Fill = ((value >> 3) % 2 == 1) ? Brushes.White : DisabledGray;
        }

        private static void LimitBits(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = (BinDecimal)d;
            var max = (uint)e.NewValue;
            if (max < 8)
            {
                control.BitFour.Visibility = Visibility.Hidden;
            }
            if (max < 4)
            {
                control.BitThree.Visibility = Visibility.Hidden;
            }
            if (max < 2)
            {
                control.BitTwo.Visibility = Visibility.Hidden;
            }
            if (max < 1)
            {
                control.BitOne.Visibility = Visibility.Hidden;
            }
        }

        public uint Value
        {
            set
            {
                SetValue(ValueProperty, value);
            }
            get
            {
                return (uint)GetValue(ValueProperty);
            }
        }

        public uint Max
        {
            set
            {
                SetValue(MaxProperty, value);
            }
            get
            {
                return (uint)GetValue(MaxProperty);
            }
        }
    }
}
